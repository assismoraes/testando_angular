import { Component } from '@angular/core';
import { HttpClient, HttpHeaders }    from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Aplicação Testando...';
  retorno: any;

  constructor(
    private http: HttpClient) { }

  req() {
    this.getRet();
  }

  getRet() {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InNlcnZpY2UiOiJkZWZhdWx0QGRlZmF1bHQiLCJyb2xlcyI6WyJhZG1pbiJdfSwiaWF0IjoxNTYyMjQxNjM0LCJleHAiOjE1NjI4NDY0MzR9.KwvBjDxsvKWrI4qjgKwSjOqWfyZh__dP2aFbHvlIlAM'
      })
    };    

    this.http.post("http://10.0.1.20:4466",
      {
        query: "{ factAttendances(first: 100) { authorizationNumber totalValue valueCoparticipation  hashcode fkDimBeneficiary { cardNumber nameBeneficiary } fkDimProvider { nameProvider } fkDimCity { pk_dim_city nameCity } } }"
      }, 
        httpOptions
      )
      .subscribe(
        data  => { console.log("POST Request is successful ", data); },
        error  => { console.log("Error", error); }
      );
  }
}
